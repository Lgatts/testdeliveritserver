package br.com.martins.testDeliverIt.model;

import br.com.martins.testDeliverIt.constants.PenaltyValues;
import java.math.BigDecimal;
import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

/**
 *
 * @author Lucas
 */
public class BillsToPayTest {

    @Test
    public void testCalculateCorrectedValue() {
        System.out.println("calculateCorrectedValue");

        BillsToPay instance = new BillsToPay();
        instance.setBaseValue(new BigDecimal(100));
        instance.setDelayedDays(5L);
        instance.setPenaltyRate(new BigDecimal(0.01));
        instance.setPenaltyRateByDay(new BigDecimal(0.001));

        BigDecimal expResult = new BigDecimal("101.50");
        BigDecimal result = instance.calculateCorrectedValue();       
        Assertions.assertEquals(expResult, result);
    }

    @Test
    public void testCalculateCorrectedValueNullValues() {
        System.out.println("calculateCorrectedValue_NullValues");

        BillsToPay instance = new BillsToPay();
        instance.setBaseValue(null);
        instance.setDelayedDays(5L);
        instance.setPenaltyRate(null);
        instance.setPenaltyRateByDay(new BigDecimal(0.001));

        Assertions.assertThrows(NullPointerException.class, () -> {
            instance.calculateCorrectedValue();
        });
    }

    @Test
    public void testCalculateCorrectedValueNegativeDelayedDays() {
        System.out.println("calculateCorrectedValue_NegativeDeleyedDays");

        BillsToPay instance = new BillsToPay();
        instance.setBaseValue(new BigDecimal(100));
        instance.setDelayedDays(-5L);
        instance.setPenaltyRate(new BigDecimal(0.01));
        instance.setPenaltyRateByDay(new BigDecimal(0.001));

        BigDecimal expResult = new BigDecimal("100.00");
        BigDecimal result = instance.calculateCorrectedValue();        
        Assertions.assertEquals(expResult, result);
    }

    @Test
    public void testSetPenaltiesRatesUntil3Days() {
        System.out.println("tsetPenaltiesRates_Until_3_Days");

        BillsToPay instance = new BillsToPay();     

        instance.setDueDate(LocalDate.parse("2020-01-01", DateTimeFormatter.ISO_DATE));
        instance.setPaymentDate(LocalDate.parse("2020-01-03", DateTimeFormatter.ISO_DATE));
        
        instance.setPenaltiesRates();
        
        long expResult = 2L;
        long result = instance.getDelayedDays();        
        Assertions.assertEquals(expResult, result);
                
        BigDecimal expResult2 = instance.getPenaltyRate().setScale(2);
        BigDecimal result2 = PenaltyValues.PENALTY_UNTIL_3_DAYS;
                
        Assertions.assertEquals(expResult2, result2);
        
        BigDecimal expResult3 = instance.getPenaltyRateByDay().setScale(3);
        BigDecimal result3 = PenaltyValues.PENALTY_BY_DAY_UNTIL_3_DAYS;
        
        Assertions.assertEquals(expResult3, result3);
    }
    
    @Test
    public void testSetPenaltiesRatesAbove3Days() {
        System.out.println("tsetPenaltiesRates_Above_3_Days");

        BillsToPay instance = new BillsToPay();

        instance.setDueDate(LocalDate.parse("2020-01-01", DateTimeFormatter.ISO_DATE));
        instance.setPaymentDate(LocalDate.parse("2020-01-05", DateTimeFormatter.ISO_DATE));

        instance.setPenaltiesRates();

        long expResult = 4L;
        long result = instance.getDelayedDays();
        Assertions.assertEquals(expResult, result);

        BigDecimal expResult2 = instance.getPenaltyRate().setScale(2);
        BigDecimal result2 = PenaltyValues.PENALTY_ABOVE_3_DAYS;

        Assertions.assertEquals(expResult2, result2);

        BigDecimal expResult3 = instance.getPenaltyRateByDay().setScale(3);
        BigDecimal result3 = PenaltyValues.PENALTY_BY_DAY_ABOVE_3_DAYS;

        Assertions.assertEquals(expResult3, result3);
    }
    
       @Test
    public void testSetPenaltiesRatesAbove5Days() {
        System.out.println("tsetPenaltiesRates_Above_5_Days");

        BillsToPay instance = new BillsToPay();     

        instance.setDueDate(LocalDate.parse("2020-01-01", DateTimeFormatter.ISO_DATE));
        instance.setPaymentDate(LocalDate.parse("2020-01-07", DateTimeFormatter.ISO_DATE));
        
        instance.setPenaltiesRates();
        
        long expResult = 6L;
        long result = instance.getDelayedDays();        
        Assertions.assertEquals(expResult, result);
                
        BigDecimal expResult2 = instance.getPenaltyRate().setScale(2);
        BigDecimal result2 = PenaltyValues.PENALTY_ABOVE_5_DAYS;
                
        Assertions.assertEquals(expResult2, result2);
        
        BigDecimal expResult3 = instance.getPenaltyRateByDay().setScale(3);
        BigDecimal result3 = PenaltyValues.PENALTY_BY_DAY_ABOVE_5_DAYS;
        
        Assertions.assertEquals(expResult3, result3);
    }
}
