package br.com.martins.testDeliverIt.repository;

import br.com.martins.testDeliverIt.model.BillsToPay;
import java.math.BigDecimal;
import java.time.LocalDate;
import javax.validation.ConstraintViolationException;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;
import org.springframework.boot.test.autoconfigure.orm.jpa.TestEntityManager;
import org.springframework.test.context.junit4.SpringRunner;

/**
 *
 * @author Lucas
 */
@RunWith(SpringRunner.class)
@DataJpaTest
public class BillsToPayRepositoryTest {

    @Autowired
    private TestEntityManager entityManager;

    @Autowired
    private BillsToPayRepository repository;

    @Test
    public void testPersistBillsToPay() {
        System.out.println("PersistBillsToPay");
        BillsToPay bill = new BillsToPay();

        bill.setName("bill");
        bill.setBaseValue(new BigDecimal("100"));
        bill.setDueDate(LocalDate.parse("2020-01-01"));
        bill.setPaymentDate(LocalDate.parse("2020-01-01"));
        bill.setPenaltiesRates();

        entityManager.persist(bill);
        entityManager.flush();

        BillsToPay found = repository.findById(bill.getId()).get();

        Assertions.assertEquals(bill, found);

    }

    @Test
    public void testPersistBillsToPay_nameNull() {
        System.out.println("PersistBillsToPay_nameNull");
        BillsToPay bill = new BillsToPay();

        Assertions.assertThrows(ConstraintViolationException.class, () -> {
            entityManager.persist(bill);
            entityManager.flush();
        });
    }

    @Test
    public void testPersistBillsToPay_baseValueNull() {
        System.out.println("PersistBillsToPay_baseValueNull");
        BillsToPay bill = new BillsToPay();

        bill.setName("test");
        bill.setBaseValue(null);
        
        Assertions.assertThrows(ConstraintViolationException.class, () -> {
            entityManager.persist(bill);
            entityManager.flush();
        });
    }
    
    @Test
    public void testPersistBillsToPay_dueDateNull() {
        System.out.println("PersistBillsToPay_dueDateNull");
        BillsToPay bill = new BillsToPay();

        bill.setName("test");
        bill.setDueDate(null);
        
        Assertions.assertThrows(ConstraintViolationException.class, () -> {
            entityManager.persist(bill);
            entityManager.flush();
        });
    }
    
    @Test
    public void testPersistBillsToPay_paymentDateNull() {
        System.out.println("PersistBillsToPay_paymentDateNull");
        BillsToPay bill = new BillsToPay();

        bill.setName("test");
        bill.setPaymentDate(null);
        
        Assertions.assertThrows(ConstraintViolationException.class, () -> {
            entityManager.persist(bill);
            entityManager.flush();
        });
    }
    
}
