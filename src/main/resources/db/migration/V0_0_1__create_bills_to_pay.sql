CREATE TABLE `bills_to_pay` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(45) NOT NULL,
  `base_value` decimal(15,5) NOT NULL,
  `due_date` datetime NOT NULL,
  `payment_date` datetime NOT NULL,
  `delayed_days` int(11) DEFAULT NULL,
  `penalty_rate` decimal(15,5) DEFAULT NULL,
  `penalty_rate_by_day` decimal(15,5) DEFAULT NULL,
  PRIMARY KEY (`id`)
);
