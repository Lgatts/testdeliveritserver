package br.com.martins.testDeliverIt.model;

import br.com.martins.testDeliverIt.constants.MoneyRound;
import br.com.martins.testDeliverIt.constants.PenaltyValues;
import java.io.Serializable;
import java.math.BigDecimal;
import java.time.LocalDate;
import static java.time.temporal.ChronoUnit.DAYS;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.persistence.Transient;
import javax.validation.constraints.NotNull;
import lombok.Data;

/**
 *
 * @author Lucas
 */
@Data
@Entity
@Table(name = "bills_to_pay")
public class BillsToPay implements Serializable {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @Column(name = "name")
    @NotNull(message = "Campo nome não pode ser vazio!")
    private String name;

    @Column(name = "base_value")
    @NotNull(message = "Campo valor original não pode ser vazio")
    private BigDecimal baseValue;

    @Column(name = "due_date")
    @NotNull(message = "Campo data de vencimento não pode ser vazio!")
    private LocalDate dueDate;

    @Column(name = "payment_date")
    @NotNull(message = "Campo data de pagamento não pode ser vazio!")
    private LocalDate paymentDate;

    @Column(name = "delayed_days")
    private Long delayedDays;

    @Column(name = "penalty_rate")
    private BigDecimal penaltyRate;

    @Column(name = "penalty_rate_by_day")
    private BigDecimal penaltyRateByDay;

    @Transient
    BigDecimal correctedValue;

    public BillsToPay() {
        baseValue = BigDecimal.ZERO;
        dueDate = LocalDate.now();
        paymentDate = LocalDate.now();
        penaltyRate = BigDecimal.ZERO;
        penaltyRateByDay = BigDecimal.ZERO;
        delayedDays = 0L;
        correctedValue = BigDecimal.ZERO;
    }

    public BigDecimal calculateCorrectedValue() {
        if (delayedDays != 0) {
            BigDecimal totalPenaltyByDay = new BigDecimal(delayedDays).multiply(penaltyRateByDay).multiply(baseValue);
            BigDecimal penaltyValue = baseValue.multiply(penaltyRate);
            correctedValue = baseValue.add(totalPenaltyByDay).add(penaltyValue);
        } else {
            correctedValue = baseValue;
        }
        return correctedValue.setScale(2, MoneyRound.ROUNDING_MODE);
    }

    public BigDecimal getCorrectedValue() {
        correctedValue = calculateCorrectedValue();
        return correctedValue;
    }

    public void setDelayedDays(long value) {
        if (value < 0) {
            this.delayedDays = 0L;
        } else {
            this.delayedDays = value;
        }
    }

    public void setPenaltiesRates() {
        if (this.getDueDate() != null || this.getPaymentDate() != null) {
            long delayDays = DAYS.between(this.getDueDate(), this.getPaymentDate());
            this.setDelayedDays(delayDays);
            if (delayDays <= 0) {
                this.setDelayedDays(0L);
            } else if (delayDays <= 3) {
                this.setPenaltyRate(PenaltyValues.PENALTY_UNTIL_3_DAYS);
                this.setPenaltyRateByDay(PenaltyValues.PENALTY_BY_DAY_UNTIL_3_DAYS);
            } else if (delayDays <= 5) {
                this.setPenaltyRate(PenaltyValues.PENALTY_ABOVE_3_DAYS);
                this.setPenaltyRateByDay(PenaltyValues.PENALTY_BY_DAY_ABOVE_3_DAYS);
            } else {
                this.setPenaltyRate(PenaltyValues.PENALTY_ABOVE_5_DAYS);
                this.setPenaltyRateByDay(PenaltyValues.PENALTY_BY_DAY_ABOVE_5_DAYS);
            }
        }
    }

}
