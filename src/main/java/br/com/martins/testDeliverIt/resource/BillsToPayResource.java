package br.com.martins.testDeliverIt.resource;

import br.com.martins.testDeliverIt.model.BillsToPay;
import br.com.martins.testDeliverIt.service.BillsToPayService;
import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 *
 * @author Lucas
 */
@RestController
@RequestMapping({"/billstopay"})
public class BillsToPayResource {

    @Autowired
    private BillsToPayService service;

    @GetMapping("/all")
    public ResponseEntity<List<BillsToPay>> getAllBillsToPay() {
        return ResponseEntity.ok(service.loadAll());
    }

    @PostMapping
    public ResponseEntity create(@RequestBody BillsToPay billsToPay) {      
        return service.save(billsToPay);
    }

    @DeleteMapping
    public HttpStatus delete(@RequestBody BillsToPay billsToPay) {        
        service.delete(billsToPay);        
        return HttpStatus.NO_CONTENT;
    }


}
