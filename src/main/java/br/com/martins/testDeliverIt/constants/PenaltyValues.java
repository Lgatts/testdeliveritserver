package br.com.martins.testDeliverIt.constants;

import java.math.BigDecimal;

/**
 *
 * @author Lucas
 */
public class PenaltyValues {
    
    public static final BigDecimal PENALTY_UNTIL_3_DAYS = new BigDecimal("0.02");
    
    public static final BigDecimal PENALTY_ABOVE_3_DAYS = new BigDecimal("0.03");
    
    public static final BigDecimal PENALTY_ABOVE_5_DAYS = new BigDecimal("0.05");
    
    public static final BigDecimal PENALTY_BY_DAY_UNTIL_3_DAYS = new BigDecimal("0.001");
    
    public static final BigDecimal PENALTY_BY_DAY_ABOVE_3_DAYS = new BigDecimal("0.002");
    
    public static final BigDecimal PENALTY_BY_DAY_ABOVE_5_DAYS = new BigDecimal("0.003");
    
}
