package br.com.martins.testDeliverIt.constants;

import java.math.RoundingMode;

/**
 *
 * @author Lucas
 */
public class MoneyRound {
    public static final RoundingMode ROUNDING_MODE = RoundingMode.HALF_EVEN;
}
