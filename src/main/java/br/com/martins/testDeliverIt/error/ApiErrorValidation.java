package br.com.martins.testDeliverIt.error;

import java.time.LocalDateTime;
import lombok.AllArgsConstructor;
import lombok.Data;

/**
 *
 * @author Lucas
 */
@AllArgsConstructor
@Data
public class ApiErrorValidation {
    
    private String message;
    private LocalDateTime errorTime;
    private String entityClassName;
    
}
