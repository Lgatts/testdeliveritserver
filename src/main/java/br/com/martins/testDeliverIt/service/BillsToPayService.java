package br.com.martins.testDeliverIt.service;

import br.com.martins.testDeliverIt.error.ApiErrorValidation;
import br.com.martins.testDeliverIt.model.BillsToPay;
import br.com.martins.testDeliverIt.repository.BillsToPayRepository;
import java.math.BigDecimal;
import java.time.LocalDateTime;
import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;

/**
 *
 * @author Lucas
 */
@Service
public class BillsToPayService {

    @Autowired
    private BillsToPayRepository repository;

    public ResponseEntity save(BillsToPay billsToPay) {

        ApiErrorValidation apiErrorValidation = validateFields(billsToPay);

        if (apiErrorValidation == null) {
            billsToPay.setPenaltiesRates();
            repository.save(billsToPay);
        }

        if (billsToPay.getId() != null) {
            return ResponseEntity.ok(billsToPay);
        } else {
            return ResponseEntity.badRequest().body(apiErrorValidation);
        }
    }

    public void delete(BillsToPay billsToPay) {
        repository.delete(billsToPay);
    }

    public List<BillsToPay> loadAll() {
        return repository.findAll();
    }

    private ApiErrorValidation validateFields(BillsToPay billsToPay) {

        StringBuilder errorBuilder = new StringBuilder();

        if (billsToPay.getBaseValue() == null || billsToPay.getBaseValue().compareTo(BigDecimal.ZERO) <= 0) {
            errorBuilder.append("O campo valor base deve ser maior que 0.0");
            errorBuilder.append(System.getProperty("line.separator"));
        }

        if (billsToPay.getDueDate() == null) {
            errorBuilder.append("O campo data de vencimento não pode ser vazio");
            errorBuilder.append(System.getProperty("line.separator"));
        }

        if (billsToPay.getPaymentDate() == null) {
            errorBuilder.append("O campo data de pagamento não pode ser vazio");
            errorBuilder.append(System.getProperty("line.separator"));
        }

        if (billsToPay.getName() == null) {
            errorBuilder.append("O campo nome não pode ser vazio");
        }

        if (errorBuilder.toString().equals("")) {
            return null;
        } else {
            return new ApiErrorValidation(errorBuilder.toString(), LocalDateTime.now(), billsToPay.getClass().getName());
        }
    }

}
