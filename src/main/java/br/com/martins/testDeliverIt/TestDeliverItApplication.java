package br.com.martins.testDeliverIt;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class TestDeliverItApplication {

    public static void main(String[] args) {
        SpringApplication.run(TestDeliverItApplication.class, args);
    }

}
