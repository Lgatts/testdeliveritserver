package br.com.martins.testDeliverIt.repository;

import br.com.martins.testDeliverIt.model.BillsToPay;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

/**
 *
 * @author Lucas
 */
@Repository
public interface BillsToPayRepository extends JpaRepository<BillsToPay, Long>{    
}
    
    
    

